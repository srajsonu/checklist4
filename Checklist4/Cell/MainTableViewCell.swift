//
//  MainTableViewCell.swift
//  Checklist4
//
//  Created by ARY@N on 07/10/19.
//  Copyright © 2019 Hitachi Payment Services. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var buttonStack: UIStackView!
    
    var dataArray: [Checklist] = []
    var respType: [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: "ChkboxCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ChkboxCollectionViewCell")
        collectionView.register(UINib(nibName: "ButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ButtonCollectionViewCell")
        collectionView.register(UINib(nibName: "ChkboxCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ChkboxCollectionViewCell")
        collectionView.register(UINib(nibName: "DropdownCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DropdownCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func responseType(response: [String]){
        respType = response
    }
    
}
extension MainTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let checkBox = collectionView.dequeueReusableCell(withReuseIdentifier: "ChkboxCollectionViewCell", for: indexPath) as! ChkboxCollectionViewCell
        collectionView.reloadData()
        return checkBox
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: contentView.frame.width, height: 77)
    }
}

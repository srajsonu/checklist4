//
//  ViewController.swift
//  Checklist4
//
//  Created by ARY@N on 07/10/19.
//  Copyright © 2019 Hitachi Payment Services. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let url = "https://fieldtrak.hitachi-payments.com/FieldTrak/API/GetAllChecklistsByIL"
    let realm = try! Realm()
    let database = Database()
    var db: Results<Database>?
    var dataArray: [Checklist] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "MainTableViewCell", bundle: nil), forCellReuseIdentifier: "MainTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 178
        db = realm.objects(Database.self)
        //fetch()
        serializeData()
    }
    func fetch(){
        let params: Parameters = [
            "user_id" : "278",
            "business_unit_id" : "2"
        ]
        Alamofire.request(url, method: .post,parameters: params).responseJSON { (response) in
            if response.result.isSuccess{
                print("API Successfully Hit")
                let apiData = JSON(response.result.value!)
                let data = apiData["data"]
                //print("data",data[0])
                data.array?.forEach({ (respond) in
                    let checklist = checklistJSON()
                    checklist.checklist_id = respond["checklist_id"].intValue
                    checklist.version_no = respond["version_no"].intValue
                    checklist.checklist_json = respond["checklist_json"].stringValue
                    checklist.app_menu_code = respond["app_menu_code"].intValue
                    checklist.bank_ids = respond["bank_ids"].intValue
                    checklist.model_ids = respond["model_ids"].intValue
                    checklist.modify_ts = respond["modify_ts"].stringValue
//                    self.check.append(checklist)
                    self.saveToDatabase(data: checklist)
                })
            }
        }
    }
    func serializeData(){
     let jsonData: Data = Data((db?[1].checklist_json.utf8)!)
     let decoder = JSONDecoder()
         do {
             let welcome = try decoder.decode([Checklist].self, from: jsonData)
             welcome.forEach({ (Checklist) in
                 dataArray.append(Checklist)
             })
             //print("data",dataArray)
        } catch {
             print(error.localizedDescription)
        }
    }
    func saveToDatabase(data: checklistJSON){
        try! self.realm.write {
            print("Data Save to database")
            let database = Database()
            database.checklist_id = data.checklist_id
            database.version_no = data.version_no
            database.checklist_json = data.checklist_json
            database.app_menu_code = data.app_menu_code
            database.bank_ids = data.bank_ids
            database.model_ids = data.model_ids
            database.modify_ts = data.modify_ts
            realm.add(database)
        }
    }
}
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let respType = dataArray[indexPath.row].responseType
        let ques = dataArray[indexPath.row].quesContent
        //let resp = dataArray[indexPath.row].responses
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell", for: indexPath) as! MainTableViewCell
        cell.label.text = ques
        cell.label.numberOfLines = 0
        cell.collectionView.backgroundColor = .gray
        
        switch respType {
        case "BUTTON":
            cell.collectionView.isHidden = false
            cell.buttonStack.isHidden = true
//            cell.frame = tableView.bounds
//            cell.layoutIfNeeded()
//            cell.collectionView.reloadData()
            cell.collectionViewHeight.constant = cell.collectionView.collectionViewLayout.collectionViewContentSize.height
            cell.layoutIfNeeded()
            //cell.collectionView.reloadData()
        case "TXTBOX":
            cell.collectionView.isHidden = false
            cell.buttonStack.isHidden = true
        case "DRPDWN":
            cell.collectionView.isHidden = false
            cell.buttonStack.isHidden = true
        case "CHKBOX":
            cell.collectionView.isHidden = false
            cell.buttonStack.isHidden = true
        case "IMAGE":
            cell.collectionView.isHidden = true
            cell.buttonStack.isHidden = false
        default:
            break
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 215
    }
}


//
//  Database.swift
//  Checklist2
//
//  Created by ARY@N on 11/09/19.
//  Copyright © 2019 Hitachi Payment  Services. All rights reserved.
//

import Foundation
import RealmSwift

class Database: Object, Decodable {
    @objc dynamic var checklist_id: Int = 0
    @objc dynamic var version_no: Int = 0
    @objc dynamic var checklist_json: String = ""
    @objc dynamic var app_menu_code: Int = 0
    @objc dynamic var bank_ids: Int = 0
    @objc dynamic var model_ids: Int = 0
    @objc dynamic var modify_ts: String = ""
}

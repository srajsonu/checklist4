//
//  Model.swift
//  Checklist2
//
//  Created by ARY@N on 11/09/19.
//  Copyright © 2019 Hitachi Payment  Services. All rights reserved.
//

import Foundation

// MARK: - Welcome
class Welcome: Decodable  {
    let flag: String?
    let data: [Datas]?
    
    init(flag: String, data: [Datas]) {
        self.flag = flag
        self.data = data
    }
}
// MARK: - Data
class Datas: Decodable {
    let checklist_id: Int?
    let version_no: Int?
    let checklist_json: Checklist?
    let app_menu_code: Int?
    let bank_ids: Int?
    let model_ids: Int?
    let modify_ts: String?
    init(checklist_id: Int, version_no: Int, checklist_json: Checklist, app_menu_code: Int, bank_ids: Int, model_ids: Int, modify_ts: String) {
        self.checklist_id = checklist_id
        self.version_no = version_no
        self.checklist_json = checklist_json
        self.app_menu_code = app_menu_code
        self.bank_ids = bank_ids
        self.model_ids = model_ids
        self.modify_ts = modify_ts
    }
}
// MARK: - Checklist
class Checklist: Decodable {
    
    let answer: String?
    let hasRemarks, isCompulsory: Bool
    let order: Int?
    let quesContent, questionNo, responseType: String
    let responses: [Response]?
    let strCurrentIDImage1, strCurrentIDImage2, strCurrentIDImage3: String?
    
    init(answer: String, hasRemarks: Bool, isCompulsory: Bool, order: Int, quesContent: String, questionNo: String, responseType: String, responses: [Response], strCurrentIDImage1: String, strCurrentIDImage2: String, strCurrentIDImage3: String) {
        self.answer = answer
        self.hasRemarks = hasRemarks
        self.isCompulsory = isCompulsory
        self.order = order
        self.quesContent = quesContent
        self.questionNo = questionNo
        self.responseType = responseType
        self.responses = responses
        self.strCurrentIDImage1 = strCurrentIDImage1
        self.strCurrentIDImage2 = strCurrentIDImage2
        self.strCurrentIDImage3 = strCurrentIDImage3
    }
}
// MARK: - Response
class Response: Decodable {
    
    let hasImage, hasSubQuestion, isUnfavourable: Bool?
    let responseText, inputType: String?
    let subCallID: Int?
    let subcall: String?
    let componentID: Int?
    let strCurrentIDImage1, strCurrentIDImage2, strCurrentIDImage3 : String?
    let subQuestion: [Checklist]?
    
    init(hasImage: Bool, hasSubQuestion: Bool, isUnfavourable: Bool, responseText: String, inputType: String, subCallID: Int, subcall: String, componentID: Int, strCurrentIDImage1: String, strCurrentIDImage2: String, strCurrentIDImage3: String, subQuestion: [Checklist]) {
        self.hasImage = hasImage
        self.hasSubQuestion = hasSubQuestion
        self.isUnfavourable = isUnfavourable
        self.responseText = responseText
        self.inputType = inputType
        self.subCallID = subCallID
        self.subcall = subcall
        self.componentID = componentID
        self.strCurrentIDImage1 = strCurrentIDImage1
        self.strCurrentIDImage2 = strCurrentIDImage2
        self.strCurrentIDImage3 = strCurrentIDImage3
        self.subQuestion = subQuestion
    }
}
//MARK: - Data
class checklistJSON {
    var checklist_id: Int = 0
    var version_no: Int = 0
    var checklist_json: String = ""
    var app_menu_code: Int = 0
    var bank_ids: Int = 0
    var model_ids: Int = 0
    var modify_ts: String = ""

}
